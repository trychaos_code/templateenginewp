var path = require("path");

module.exports = {
  entry: __dirname+"/app.js",
  output: {
    path: __dirname + "/dist",
    filename: "bundle.js"
  },
  resolve: {
  },
  module: {
    rules: [{ test: /\.handlebars$/, loader: __dirname + "/../../" }]
  }
};
